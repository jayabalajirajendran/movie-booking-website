import os
from datetime import timedelta

from flask import Flask
from source.API.Movies import movieApi
from source.API.Login import loginApi
from source.API.User import userApi
from source.API.Admin import adminApi

# Check the path for the template and static folder
app = Flask(__name__, template_folder='/Users/jb/LocalDisk/DBS/Semester 1/Information Systems/DBS-Movie-Theater/webapp',
            static_folder='/Users/jb/LocalDisk/DBS/Semester 1/Information Systems/DBS-Movie-Theater/webapp/static')

app.register_blueprint(movieApi)
app.register_blueprint(loginApi)
app.register_blueprint(userApi)
app.register_blueprint(adminApi)

# Python Flask Main Function
if __name__ == '__main__':
    app.secret_key = os.urandom(12)
    app.permanent_session_lifetime = timedelta(minutes=20)
    app.run(debug=True, threaded=True, host='0.0.0.0', port=80)
