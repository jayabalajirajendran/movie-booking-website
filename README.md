Scenario:

Create an Information System for a selected domain of interest. 
You may use any back-end, including a DB developed in another module. 
You may use any front-end, including CLI, GUI, web and API.
Describe the requirements of the information system, including users, data requirements, search, sorting, entry, update, validation, integrity, reporting etc.
Implement and test the Information System, and document your implementation thoroughly.
You must use public git (e.g. GitHub) to manage your source and version, with regular frequent commits. Git may be used to verify engagement and contribution, and failure to engage may result in a zero grade.
You must attribute all code not written from scratch, either in accordance with its licence, if applicable, or if not, #taken from ...  failure to do so may result in a zero grade.
You may use any programming language, however example programs will be presented in Python.

Group 6: Chandana(10539269), Jayabalaji(10537723), Kingsley(10527084)


Considering Movie Booking system.
Planning to implement using SQLite with Flask user UI
Considering and assessing the feasibility of using TKInter for Admin interface

Research material:

https://dev.mysql.com/doc/connector-python/en/connector-python-example-connecting.html
https://flask.palletsprojects.com/en/1.1.x/
https://flask.palletsprojects.com/en/1.1.x/blueprints/
https://jinja.palletsprojects.com/en/2.11.x/api/

<br>

We will be using <br><br>
MySQL as Backend Database - Jayabalaji(10537723)<br>
Python Flask as API Integration - Chandana(10539269)<br>
HTML,CSS,JavaScript and jQuery as FrontEnd - Kingsley(10527084)

Scenario: Movie booking system
User requirement: Movie Time system displays movies, their timings and availability of seats based on genres, languages and location, and allows the user to book a movie over the internet. 

MOM1: 
> sqlite3 local db trial code
> Todo: mysql with actual VM

MOM2:
> MySQL base code has to be updated.
> DB - Flask - Frontend
> Admin

MOM3 & MOM4:
> MySQL base code has updated.
> Everyone uses their local DB and retain one local DB in Github for time being.
> Everyone pushes their changes related to fetch movie details functionality by 5th Apr EOD.

MOM5
>Integrating Create users, admin, update movies, delete movies

MOM6
>Integrated code push by 15th Apr EOD
