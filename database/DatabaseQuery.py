from database import DatabaseConnection


# Fetch all the movies from Database
def getAllMovie():
    cur = DatabaseConnection.connection().cursor()
    cur.execute("SELECT * FROM movie")
    data = cur.fetchall()
    cur.close()
    return data


# Add movie to Database
def addMovie(tableName, movieName, directorName, genre, description, image, storyPlot):
    db_connection = DatabaseConnection.connection()
    cur = db_connection.cursor()
    cur.execute(
        "INSERT INTO " + tableName + "(Genre,MovieName,DirectorName,Description,Image,Para) VALUES (%s,%s,%s,%s,%s,%s)",
        (genre, movieName, directorName, description, image, storyPlot))
    db_connection.commit()
    cur.close()


# Fetch Movie using Movie ID
def getMovieUsingId(movieId):
    db_connection = DatabaseConnection.connection()
    cur = db_connection.cursor()
    cur.execute("SELECT * FROM movie where id=" + movieId)
    data = cur.fetchone()
    db_connection.commit()
    cur.close()
    return data


# Delete Movie using Movie ID
def deleteMovieUsingId(movieId):
    db_connection = DatabaseConnection.connection()
    cur = db_connection.cursor()
    cur.execute("Delete from movie where id=" + movieId)
    db_connection.commit()
    cur.close()


# Update Movie using Movie ID
def updateMovie(movieId, movieName, directorName, genre, description, image, storyPlot):
    db_connection = DatabaseConnection.connection()
    cur = db_connection.cursor()
    cur.execute(
        "UPDATE movie SET movieName=%s,directorName=%s,genre=%s,description=%s,image=%s,para=%s where id =%s",
        (movieName, directorName, genre, description, image, storyPlot, movieId))
    db_connection.commit()
    cur.close()


# Create User in Database
def createUser(name, userName, email, gender, dateofbirth, phoneNumber, password):
    db_connection = DatabaseConnection.connection()
    cur = db_connection.cursor()
    cur.execute(
        "INSERT INTO user (name,username,Password,Email,Gender,DateOfBirth,PhoneNumber) VALUES (%s,%s,%s,%s,%s,%s,%s)",
        (name, userName, email, gender, dateofbirth, phoneNumber, password))
    db_connection.commit()
    cur.close()


# Get User by User Name
def getUserByUserName(userName):
    db_connection = DatabaseConnection.connection()
    cur = db_connection.cursor()
    cur.execute(
        "Select * from user where username=%s", userName)
    data = cur.fetchone()
    db_connection.commit()
    cur.close()
    return data


def getAllUser():
    db_connection = DatabaseConnection.connection()
    cur = db_connection.cursor()
    cur.execute(
        "Select * from user ")
    data = cur.fetchall()
    db_connection.commit()
    cur.close()
    return data


def updateTicketHistory(movieList, userName):
    db_connection = DatabaseConnection.connection()
    cur = db_connection.cursor()
    cur.execute(
        "update user set BookedMovies='{0}' where UserName='{1}'".format(movieList, userName))
    db_connection.commit()
    cur.close()
