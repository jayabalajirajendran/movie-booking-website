CREATE DATABASE  IF NOT EXISTS `Entertainment` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `Entertainment`;
-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: localhost    Database: Entertainment
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movie` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `Genre` varchar(20) NOT NULL,
  `MovieName` varchar(255) NOT NULL,
  `DirectorName` varchar(255) DEFAULT NULL,
  `Description` longtext,
  `Image` varchar(255) DEFAULT NULL,
  `Para` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'Romantic','Titanic','James Cameron','Seventeen-year-old Rose hails from an aristocratic family and is set to be married. When she boards the Titanic, she meets Jack Dawson, an artist, and falls in love with him.','https://titanicsound.files.wordpress.com/2014/11/titanic_movie-hd-1.jpg',''),(2,'Thriller','Speed','Jan de Bont','A disgruntled, dangerous man plants a bomb in an elevator. When his mission fails, he plants a bomb in a local bus and threatens to set it off unless his demand is met.','https://lumiere-a.akamaihd.net/v1/images/speed_feature-poster_584x800_6f42936d.jpeg','A disgruntled, dangerous man plants a bomb in an elevator. When his mission fails, he plants a bomb in a local bus and threatens to set it off unless his demand is met.'),(3,'Action','Avengers Endgame','Joe Russo, Anthony Russo','After Thanos, an intergalactic warlord, disintegrates half of the universe, the Avengers must reunite and assemble again to reinvigorate their trounced allies and restore balance.','https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fimage%2F2019%2F03%2Fmarvel-official-avengers-endgame-plot-synopsis-1.jpg?q=75&w=800&cbr=1&fit=max','Adrift in space with no food or water, Tony Stark sends a message to Pepper Potts as his oxygen supply starts to dwindle. Meanwhile, the remaining Avengers -- Thor, Black Widow, Captain America and Bruce Banner -- must figure out a way to bring back their vanquished allies for an epic showdown with Thanos -- the evil demigod who decimated the planet and the universe.');
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `LoginId` int(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `UserName` varchar(20) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Gender` varchar(255) DEFAULT NULL,
  `DateOfBirth` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(255) DEFAULT NULL,
  `BookedMovies` longtext,
  PRIMARY KEY (`LoginId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin','406b8ee8fe6080d83984ddbf77e15a09fcec85c51e3596b1af06382feb73b3b54d316ceacd10e02085811777b793c5454d51b429ddf28a6a8e6d2c3ca64a4ddb77f3d6d4310d82a7afd7559f9f31aaae25e3dca17cfddb77f0cb9fde1de80e67','admin@movies.com','m',NULL,NULL,NULL),(2,'Jayabalaji','jay','a3b524eca53ec4674c48d06678427a12b00f900b6352fc3e533cc7f91a4e74e017e66095afb8e9540c87d54c61c80d80b64d8e942340dfe3cfcae56fd08203154fdb8d3e68c8adaaef62a70ce8b3ab33038ea605cea824f5995d558e234be8eb','jaya@gmail.com','m','2021-02-01','0872417020','[{\"id\": 2, \"genre\": \"Thriller\", \"movieName\": \"Speed\", \"directorName\": \"Jan de Bont\", \"description\": \"A disgruntled, dangerous man plants a bomb in an elevator. When his mission fails, he plants a bomb in a local bus and threatens to set it off unless his demand is met.\", \"image\": \"https://lumiere-a.akamaihd.net/v1/images/speed_feature-poster_584x800_6f42936d.jpeg\", \"movieDate\": \"02/07/2021\", \"numberOfSeats\": \"2\", \"seatNumber\": \"A6,A7\", \"bookedBy\": \"jay\", \"timeStamp\": \"2021-02-07T11:21:17\"}, {\"id\": 1, \"genre\": \"Romantic\", \"movieName\": \"Titanic\", \"directorName\": \"James Cameron\", \"description\": \"Seventeen-year-old Rose hails from an aristocratic family and is set to be married. When she boards the Titanic, she meets Jack Dawson, an artist, and falls in love with him.\", \"image\": \"https://titanicsound.files.wordpress.com/2014/11/titanic_movie-hd-1.jpg\", \"movieDate\": \"02/07/2021\", \"numberOfSeats\": \"1\", \"seatNumber\": \"A6\", \"bookedBy\": \"jay\", \"timeStamp\": \"2021-02-07T11:25:36\"}, {\"id\": 1, \"genre\": \"Romantic\", \"movieName\": \"Titanic\", \"directorName\": \"James Cameron\", \"description\": \"Seventeen-year-old Rose hails from an aristocratic family and is set to be married. When she boards the Titanic, she meets Jack Dawson, an artist, and falls in love with him.\", \"image\": \"https://titanicsound.files.wordpress.com/2014/11/titanic_movie-hd-1.jpg\", \"movieDate\": \"02/07/2021\", \"numberOfSeats\": \"3\", \"seatNumber\": \"F6,F7,F8\", \"bookedBy\": \"jay\", \"timeStamp\": \"2021-02-07T11:26:10\"}]');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-07 12:55:22
