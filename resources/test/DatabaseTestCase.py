import unittest
import os
import sqlalchemy

DB_TEST_URL = "jdbc:mysql://localhost:3306"


# Test case reference based on https://julien.danjou.info/db-integration-testing-strategies-python/
class TestDB(unittest.TestCase):
    def createDatabase(self):
        url = os.getenv("DB_TEST_URL")
        if not url:
            self.skipTest("No database URL set")
        self.engine = sqlalchemy.create_engine(url)
        self.connection = self.engine.connect()
        self.connection.execute("CREATE DATABASE testdb")

    def drop(self):
        self.connection.execute("DROP DATABASE testdb")

    def createTable(self):
        self.connection.execute("CREATE TABLE test(ID int,name varchar(50),details varchar(100))")

    def insertData(self):
        self.connection.execute("INSERT INTO test('ID','name','details') VALUES('1','balaji', 'testing')")

    def deleteData(self):
        self.connection.execute("Delete from test where id='1'")

    def getDataByID(self):
        self.connection.execute("Select * from test where id='1'")

    def getAllData(self):
        self.connection.execute("Select * from test ")

    def updateData(self):
        self.connection.execute("update test set details='Update Testing' where ID='1'")
