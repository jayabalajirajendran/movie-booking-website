from flask import Flask
import unittest
import os
from datetime import timedelta
from source.API.Movies import movieApi

app = Flask(__name__, template_folder='../../webapp/',
            static_folder='../../webapp/static')
app.register_blueprint(movieApi, url_prefix='')

#https://stackoverflow.com/questions/23987564/test-flask-render-template-context
class BluePrintTestCase(unittest.TestCase):

    def setUp(self):
        print("Setup invoked")
        self.app = app.test_client()

    def test_home(self):
        print("Home Loaded successfully")
        rv = self.app.get('/')
        # print(rv.data)
        self.assertEqual(200, rv.status_code)

    def test_add(self):
        print("Add HTML loaded successfully")
        rv = self.app.get('/add')
        self.assertEqual(200, rv.status_code)

    def test_update(self):
        print("Update successfully")
        rv = self.app.get('/update')
        self.assertEqual(200, rv.status_code)



if __name__ == '__main__':
    app.secret_key = os.urandom(12)
    app.permanent_session_lifetime = timedelta(minutes=20)
    unittest.main()

