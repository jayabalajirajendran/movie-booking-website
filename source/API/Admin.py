from flask import render_template, flash, Blueprint, redirect
from source.Model.BookingHistory import BookingHistory
from source.Model.MovieBrief import MovieBrief
from source.Model.UserDetails import UserDetails

from database import DatabaseQuery
import json

adminApi = Blueprint('adminApi', __name__)


# Python Flask API to Show Admin Statics
@adminApi.route('/admin')
def adminStatistics():
    try:
        dataRecords = {}
        movie_data = DatabaseQuery.getAllMovie()
        movieList = []
        userList = []
        bookingTransaction = []
        user_data = DatabaseQuery.getAllUser()
        dataRecords["superAdmin"] = "true"
        for userData in user_data:
            userDetails = UserDetails(userData[0], userData[1], userData[2], userData[3], userData[4], userData[5],
                                      userData[6],
                                      userData[7], userData[8])
            if userDetails.getUserName() is not "admin":
                jsonDump = json.dumps(userDetails.__dict__)
                userList.append(json.loads(jsonDump))
            if userDetails.getBookedMovies() is not None:
                for value in json.loads(userDetails.getBookedMovies()):
                    bookingHistory = BookingHistory(value['id'], value['genre'], value['movieName'],
                                                    value['directorName'],
                                                    value['description'], value['image'],
                                                    value['movieDate'], value['numberOfSeats'], value['seatNumber'],
                                                    value['bookedBy'], value['timeStamp'])
                    jsonDump = json.dumps(bookingHistory.__dict__)
                    bookingTransaction.append(json.loads(jsonDump))
        for movieData in movie_data:
            movieBrief = MovieBrief(movieData[0], [movieData[1]], movieData[2], movieData[3], movieData[4],
                                    movieData[5],
                                    movieData[6])
            jsonDump = json.dumps(movieBrief.__dict__)
            movieList.append(json.loads(jsonDump))
        dataRecords['movieList'] = movieList
        dataRecords['userList'] = userList
        dataRecords['bookingTransaction'] = bookingTransaction
        return render_template('admin.html', data=json.dumps(dataRecords))
    except:
        flash('Something went wrong')
        return redirect("/")
