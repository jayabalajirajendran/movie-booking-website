from flask import render_template, request, flash, redirect, session, Blueprint
from database import DatabaseQuery
from source.Model.UserDetails import UserDetails
import binascii
import hashlib

loginApi = Blueprint('loginApi', __name__)


# Creating Session taken from the reference https://pythonhosted.org/Flask-Session/
# Python Flask API for login
@loginApi.route('/login', methods=['GET', 'POST'])
def loginPage():
    try:
        if request.method == "POST":
            userName = request.form["username"]
            password = request.form["password"]
            postAction = request.form["postAction"]
            if postAction == 'create':
                return redirect("/create")
            userDetailsFromDB = DatabaseQuery.getUserByUserName(userName)
            if userDetailsFromDB:
                # Based on Database Details, Passing it in UserDetails Class
                userDetails = UserDetails(userDetailsFromDB[0], userDetailsFromDB[1], userDetailsFromDB[2],
                                          userDetailsFromDB[3], userDetailsFromDB[4], userDetailsFromDB[5],
                                          userDetailsFromDB[6], userDetailsFromDB[7], userDetailsFromDB[8])
                if verify_password(password, userDetails.getPassword()):
                    if userName == 'admin' and password == 'admin':
                        session['superAdmin'] = True
                    else:
                        session['logged_in'] = True
                    session['userName'] = userName
                    return redirect("/")
                else:
                    flash('Please enter a valid password')
                    return redirect("/login")
            else:
                flash('No Username and Password found in our records, Please try again')
                return redirect("/login")
        return render_template('login.html')
    except:
        flash('Something went wrong')
        return redirect("/")


# Python Flask API for Logout
@loginApi.route("/logout")
def logout():
    try:
        if session.get("superAdmin"):
            session['superAdmin'] = False
        else:
            session['logged_in'] = False
        return redirect("/")
    except:
        flash('Something went wrong')
        return redirect("/")


def verify_password(password, database_password):
    salt = database_password[:64]
    stored_password = database_password[64:]
    databasePassword_hash = hashlib.pbkdf2_hmac('sha512',
                                                password.encode('utf-8'),
                                                salt.encode('ascii'),
                                                100000)
    databasePassword_hash = binascii.hexlify(databasePassword_hash).decode('ascii')
    return databasePassword_hash == stored_password
