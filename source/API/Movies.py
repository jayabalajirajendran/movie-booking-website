from datetime import datetime
from flask import render_template, session, request, redirect, Blueprint, flash, jsonify
from source.Model.BookingHistory import BookingHistory
from source.Model.MovieBrief import MovieBrief
from source.Model.UserDetails import UserDetails
from database import DatabaseQuery
import json

movieApi = Blueprint('movieApi', __name__)


# Python Flask API for home page
@movieApi.route('/')
def homePage():
    try:
        login_dictionary = {}
        data = DatabaseQuery.getAllMovie()
        movie_list = []
        if session.get('logged_in'):
            login_dictionary['logged_in'] = "true"
        elif session.get('superAdmin'):
            login_dictionary["superAdmin"] = "true"
        # Fetch All the Movies from Database
        for movieFromDB in data:
            movieBrief = MovieBrief(movieFromDB[0], [movieFromDB[1]], movieFromDB[2], movieFromDB[3], movieFromDB[4],
                                    movieFromDB[5], movieFromDB[6])
            jsonData = json.dumps(movieBrief.__dict__)
            movie_list.append(json.loads(jsonData))
        login_dictionary['movieList'] = json.dumps(movie_list)
        return render_template('index.html', data=login_dictionary)
    except:
        flash('Something went wrong')
        return redirect("/")


# Python Flask API for adding an movie
@movieApi.route('/add', methods=['GET', 'POST'])
def addMovie():
    try:
        if request.method == "POST":
            postAction = request.form["postAction"]
            if postAction == 'cancel':
                return redirect("/")
            movieName = request.form["movieName"]
            directorName = request.form["directorName"]
            genre = request.form["genre"]
            description = request.form["description"]
            image = request.form["image"]
            storyPlot = request.form["para"]
            DatabaseQuery.addMovie('movie', movieName.title(), directorName.title(), genre.title(),
                                   description, image, storyPlot)
            flash("Movie Added Successfully")
            return redirect("/")
        return render_template('add.html')
    except:
        flash('Something went wrong')
        return redirect("/")


# Python Flask API for adding an movie
@movieApi.route('/update', methods=['GET', 'POST'])
def updateMovie():
    try:
        movieId = request.args.get('id')
        movie = []
        # Fetch Movie from Database using Movie ID
        if movieId is not None:
            movieFromDB = DatabaseQuery.getMovieUsingId(movieId)
            movieBrief = MovieBrief(movieFromDB[0], [movieFromDB[1]], movieFromDB[2], movieFromDB[3], movieFromDB[4],
                                    movieFromDB[5], movieFromDB[6])
            movie.append(movieBrief.__dict__)
        else:
            if request.method == "POST":
                postAction = request.form["postAction"]
                if postAction == 'delete':
                    return deleteMovie()
                movieId = int(request.form["movieId"])
                movieName = request.form["movieName"]
                directorName = request.form["directorName"]
                genre = request.form["genre"]
                description = request.form["description"]
                para = request.form["para"]
                image = request.form["image"]
                DatabaseQuery.updateMovie(movieId, movieName, directorName, genre, description, image, para)
                flash("Movie Updated Successfully")
                return redirect("/")
        jsonData = json.dumps(movie)
        return render_template('update.html', update=json.loads(jsonData))
    except:
        flash('Something went wrong')
        return redirect("/")


# Python Flask API for ticket history
@movieApi.route('/ticketHistory')
def getTicketHistory():
    try:
        ticketHistory = {}
        if session.get('logged_in'):
            ticketHistory = {'logged_in': "true"}
        userData = DatabaseQuery.getUserByUserName(session.get("userName"))
        userDetails = UserDetails(userData[0], userData[1], userData[2],
                                  userData[3], userData[4], userData[5],
                                  userData[6], userData[7], userData[8])
        if userDetails.getBookedMovies() is not None:
            ticketHistory["movie_list"] = userDetails.getBookedMovies()
        else:
            flash('No Booking History Found')
            return redirect("/")
        return render_template('ticketHistory.html', data=ticketHistory)
    except:
        flash('Something went wrong')
        return redirect("/")


# Python Flask API for booking a movie
@movieApi.route('/bookMovie', methods=['GET', 'POST'])
def doBooking():
    try:
        if request.method == 'POST':
            userBookingHistory = DatabaseQuery.getUserByUserName(session.get("userName"))
            if userBookingHistory[8] is None:
                movieBookingList = []
            else:
                movieBookingList = json.loads(userBookingHistory[8])
            movieFromDB = DatabaseQuery.getMovieUsingId(str(request.json['MovieId']))
            bookingHistory = BookingHistory(movieFromDB[0], movieFromDB[1], movieFromDB[2], movieFromDB[3],
                                            movieFromDB[4],
                                            movieFromDB[5], request.json['MovieDate'], request.json['NumberOfSeats'],
                                            request.json['SeatNumber'], session.get("userName"),
                                            datetime.now().strftime('%Y-%m-%dT%H:%M:%S'))
            movieBookingList.append(bookingHistory.__dict__)
            DatabaseQuery.updateTicketHistory(json.dumps(movieBookingList), session.get("userName"))
            # Sending back AJAX Response Call
            return jsonify(flash("Movie Booked Successfully"))
        movieId = request.args.get('id')
        data = DatabaseQuery.getMovieUsingId(movieId)
        movieBrief = MovieBrief(data[0], data[1], data[2], data[3], data[4], data[5], data[6])
        if session.get('logged_in'):
            movieBrief.__dict__['logged_in'] = "true"
        elif session.get('superAdmin'):
            movieBrief.__dict__['superAdmin'] = "true"
        jsonData = json.dumps(movieBrief.__dict__)
        return render_template('bookMovie.html', data=json.loads(jsonData))
    except:
        flash('Something went wrong')
        return redirect("/")


# Python Flask API for booking a seat
@movieApi.route('/bookSeat', methods=['GET', 'POST'])
def seatBooking():
    try:
        bookSeat = {}
        postAction = request.form['submit']
        user_data = DatabaseQuery.getAllUser()
        bookingTransaction = []
        for userData in user_data:
            userDetails = UserDetails(userData[0], userData[1], userData[2], userData[3], userData[4], userData[5],
                                      userData[6],
                                      userData[7], userData[8])
            if userDetails.getBookedMovies() is not None:
                for value in json.loads(userDetails.getBookedMovies()):
                    bookingHistory = BookingHistory(value['id'], value['genre'], value['movieName'],
                                                    value['directorName'],
                                                    value['description'], value['image'],
                                                    value['movieDate'], value['numberOfSeats'], value['seatNumber'],
                                                    value['bookedBy'], value['timeStamp'])
                    if postAction == str(bookingHistory.getMovieId()):
                        jsonDump = json.dumps(bookingHistory.__dict__)
                        bookingTransaction.append(json.loads(jsonDump))
        if session.get('logged_in'):
            bookSeat['logged_in'] = "true"
        if postAction == 'cancel':
            return redirect("/")
        elif postAction == 'logIn':
            return redirect("/login")
        bookSeat['movieId'] = postAction
        bookSeat['bookingTransaction'] = json.dumps(bookingTransaction)
        return render_template('bookSeat.html', data=bookSeat)
    except:
        flash('Something went wrong')
        return redirect("/")


# Python Flask API for deleting a movie
@movieApi.route('/delete')
def deleteMovie():
    try:
        movieId = request.form["movieId"]
        DatabaseQuery.deleteMovieUsingId(movieId)
        flash("Movie Deleted Successfully")
        return redirect("/")
    except:
        flash('Something went wrong')
        return redirect("/")
