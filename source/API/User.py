import binascii
import hashlib
import os
from flask import render_template, request, flash, redirect, Blueprint
from database import DatabaseQuery
from source.Model.UserDetails import UserDetails

userApi = Blueprint('userApi', __name__)


# Hashing Password based on reference https://www.vitoshacademy.com/hashing-passwords-in-python/
# Python Flask API to fetch the data from MYSQL
@userApi.route('/create', methods=['GET', 'POST'])
def createUser():
    if request.method == "POST":
        user_data = DatabaseQuery.getAllUser()
        userName = request.form['userName']
        email = request.form['email']
        for userData in user_data:
            userDetails = UserDetails(userData[0], userData[1], userData[2], userData[3], userData[4], userData[5],
                                      userData[6],
                                      userData[7], userData[8])
            if userName == userDetails.getUserName():
                flash('User Name Already Taken, Please try with a different user name')
                return redirect("/create")
        gender = request.form['gender']
        date_of_birth = request.form['dob']
        name = request.form['name']
        phoneNumber = request.form['phoneNumber']
        password = request.form['password']
        DatabaseQuery.createUser(name.capitalize(), userName, hash_password(password), email, gender, date_of_birth,
                                 phoneNumber)
        flash('User Account Created Successfully')
        return redirect("/login")
    return render_template("user.html")


def hash_password(password):
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    hashPassword = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                       salt, 100000)
    hashPassword = binascii.hexlify(hashPassword)
    return (salt + hashPassword).decode('ascii')
