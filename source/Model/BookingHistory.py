class BookingHistory:
    def __init__(self, movieId, genre, movieName, directorName, description, image, movieDate, numberOfSeats,
                 seatNumber, bookedBy, timeStamp):
        self.id = movieId
        self.genre = genre
        self.movieName = movieName
        self.directorName = directorName
        self.description = description
        self.image = image
        self.movieDate = movieDate
        self.numberOfSeats = numberOfSeats
        self.seatNumber = seatNumber
        self.bookedBy = bookedBy
        self.timeStamp = timeStamp

    def getMovieId(self):
        return self.id

    def getGenre(self):
        return self.genre

    def getMovie(self):
        return self.movieName

    def getDirectorName(self):
        return self.directorName

    def getDescription(self):
        return self.description

    def getImage(self):
        return self.image

    def getMovieDate(self):
        return self.movieDate

    def getNumberOfSeats(self):
        return self.numberOfSeats

    def getSeatNumber(self):
        return self.seatNumber

    def getBookedBy(self):
        return self.bookedBy

    def getTimeStamp(self):
        return self.timeStamp
