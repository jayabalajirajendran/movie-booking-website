class MovieBrief:
    def __init__(self, movieId, genre, movieName, directorName, description, image, para):
        self.id = movieId
        self.genre = genre
        self.movieName = movieName
        self.directorName = directorName
        self.description = description
        self.image = image
        self.para = para

    def getMovieId(self):
        return self.id

    def getGenre(self):
        return self.genre

    def getMovie(self):
        return self.movieName

    def getDirectorName(self):
        return self.directorName

    def getDescription(self):
        return self.description

    def getImage(self):
        return self.image

    def getPara(self):
        return self.para
