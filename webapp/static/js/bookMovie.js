/*  Read More JS*/


(function ($) {
    $WIN = $(window);
    /* Preloader will fade out the loading animation*/
    var ssPreloader = function () {
        $WIN.on('load', function () {
            $("#loader").fadeOut("slow", function () {
                $("#preloader").delay(300).fadeOut("slow");

            });
        });
    };

    /* load json file for category */
    if (typeof data != "undefined" && data != null) {
        $('#read_value_block').append(" <article class='format-standard'> <div class='content-media'> <div class='post-thumb'> <img src='" + data['image'] + "'> </div> </div> <div class='primary-content'> <h1 class='page-title'>" + data['movieName'] + "</h1> <ul class='entry-meta'> <li class='date'>" + data['genre'] + "</li> <li class='cat'>" + data['directorName'] + "</a></li> </ul><p class='lead'>" + data['description'] + "</p>  <p>" + data['para'] + "</p><!-- end entry-primary --> </article>");
    }

    /* Mobile Menu
 ------------------------------------------------------ */
    var ssMobileNav = function () {

        var toggleButton = $('.menu-toggle'),
            nav = $('.main-navigation');

        toggleButton.on('click', function (event) {
            event.preventDefault();

            toggleButton.toggleClass('is-clicked');
            nav.slideToggle();
        });

        if (toggleButton.is(':visible')) nav.addClass('mobile');

        $WIN.resize(function () {
            if (toggleButton.is(':visible')) nav.addClass('mobile');
            else nav.removeClass('mobile');
        });

        $('#main-nav-wrap li a').on("click", function () {
            if (nav.hasClass('mobile')) {
                toggleButton.toggleClass('is-clicked');
                nav.fadeOut();
            }
        });

    };
    /*	Masonry
     ------------------------------------------------------ */
    var ssMasonryFolio = function () {
        var containerBricks = $('.bricks-wrapper');

        containerBricks.imagesLoaded(function () {

            containerBricks.masonry({
                itemSelector: '.entry',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                resize: true
            });

        });
    };

    /* Smooth Scrolling
      * ------------------------------------------------------ */
    var ssSmoothScroll = function () {

        $('.smoothscroll').on('click', function (e) {
            var target = this.hash,
                $target = $(target);

            e.preventDefault();
            e.stopPropagation();

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 800, 'swing').promise().done(function () {

                // check if menu is open
                if ($('body').hasClass('menu-is-open')) {
                    $('#header-menu-trigger').trigger('click');
                }

                window.location.hash = target;
            });
        });

    };


    /* Placeholder Plugin Settings
      * ------------------------------------------------------ */
    var ssPlaceholder = function () {
        $('input, textarea, select').placeholder();
    };


    /* Back to Top
      * ------------------------------------------------------ */
    var ssBackToTop = function () {

        var pxShow = 500,         // height on which the button will show
            fadeInTime = 400,         // how slow/fast you want the button to show
            fadeOutTime = 400,         // how slow/fast you want the button to hide
            scrollSpeed = 300,         // how slow/fast you want the button to scroll to top. can be a value, 'slow', 'normal' or 'fast'
            goTopButton = $("#go-top")

        // Show or hide the sticky footer button
        $(window).on('scroll', function () {
            if ($(window).scrollTop() >= pxShow) {
                goTopButton.fadeIn(fadeInTime);
            } else {
                goTopButton.fadeOut(fadeOutTime);
            }
        });
    };


    /* Initialize
      * ------------------------------------------------------ */
    (function ssInit() {
        ssSmoothScroll();
        ssPreloader();
        ssMobileNav();
        ssMasonryFolio();
        ssPlaceholder();
        ssBackToTop();
    })();


})(jQuery);
