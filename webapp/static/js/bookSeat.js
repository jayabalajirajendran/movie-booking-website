$(document).ready(function () {
    var dateToday = new Date();
    $("#date").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        minDate: dateToday
    });
});

var seatList = [];
var date = null;
var bookingTransaction = JSON.parse(data['bookingTransaction']);
if (bookingTransaction != null) {
    $.each(bookingTransaction, function (key, value) {
        date = value['movieDate'];
        seatNumbers = value['seatNumber'].split(',');
        seatList = seatList.concat(seatNumbers);
    });
}

function onLoaderFunc() {
    $(".seatStructure *").prop("disabled", true);
    $(".displayerBoxes *").prop("disabled", true);
}

function takeData() {
    if (($("#date").val().length == 0) || ($("#Numseats").val().length == 0)) {
        alert("Please Choose the date and Number of Seats");
    } else {
        $(".inputForm *").prop("disabled", true);
        $(".seatStructure *").prop("disabled", false);
        document.getElementById("selectingButton").outerHTML = " <button class='btn btn--primary' id='editingButton' style='line-height:10px;height: auto;margin-right:10px;' onclick='editData()'>Edit Selecting</button>"
        var userDate = $("#date").val();
        if (date === userDate) {
            seatList.forEach(function (value) {
                var elementById = document.getElementById(value);
                $(elementById).removeClass('seats');
                elementById.classList.add('redBox');
                $(elementById).attr('disabled', true);
            })
        }
        document.getElementById("notification").innerHTML = "<b style='margin-bottom:0px;background:#33ccff;text-align:center;'>Please Select your Seats NOW!</b>";
    }
}

function editData() {
    if (($("#date").val().length == 0) || ($("#Numseats").val().length == 0)) {
        alert("Please Choose the date and Number of Seats");
    } else {
        $('#editingButton').toggle(function () {
            $('input:checkbox').attr('checked', 'checked');
            $(this).val('uncheck all');
        }, function () {
            $('input:checkbox').removeAttr('checked');
            $(this).val('check all');
        });
        seatList.forEach(function (value) {
            console.log(value);
            var elementById = document.getElementById(value);
            $(elementById).removeClass('redBox');
            elementById.classList.add('seats');
        });
        $(".inputForm *").prop("disabled", false);
        $(".seatStructure *").prop("disabled", true);
        document.getElementById("editingButton").outerHTML = " <button class='btn btn--primary' id='selectingButton' style='line-height:10px;height: auto;margin-right:10px;' onclick='takeData()'>Start Selecting</button>"
        $('#notification').empty();
    }
}


function updateTextArea() {

    if ($("input:checked").length == ($("#Numseats").val())) {
        $(".seatStructure *").prop("disabled", true);
        $("#confirmSelection").attr("disabled", true);
        var allDateVals = [];
        var allNumberVals = [];
        var allSeatsVals = [];

        //Storing in Array
        allDateVals.push($("#date").val());
        allNumberVals.push($("#Numseats").val());
        $('#seatsBlock :checked').each(function () {
            allSeatsVals.push($(this).val());
        });

        //Displaying
        $('#dateDisplay').val(allDateVals);
        $('#NumberDisplay').val(allNumberVals);
        $('#seatsDisplay').val(allSeatsVals);

        $.ajax({
            type: "POST",
            url: '/bookMovie',
            dataType: "json",
            data: JSON.stringify({
                'MovieId': data['movieId'],
                'MovieDate': allDateVals.toString(),
                'NumberOfSeats': allNumberVals.toString(),
                'SeatNumber': allSeatsVals.toString()
            }),
            contentType: 'application/json;charset=UTF-8',
            success: function (data) {
                $('#flash_message').html(JSON.stringify(data, null, '   '));
                window.location.href = "/";
            },
            error: function (data) {
                console.log(data);
                $('#flash_message').html(JSON.stringify(data, null, '   '));
                window.location.href = "/";
            }
        });
    } else {
        alert("Please select " + ($("#Numseats").val()) + " seats")
    }
}


$(":checkbox").click(function () {
    var checkedBox = $(this).attr("value");
    if ($.inArray(checkedBox, seatList) != -1) {
        $('#' + checkedBox + '').prop('checked', false);
    } else {
        if ($("input:checked").length == ($("#Numseats").val())) {
            $(":checkbox").prop('disabled', true);
            $(':checked').prop('disabled', false);
        } else {
            $(":checkbox").prop('disabled', false);
        }
    }
});



